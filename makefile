# Makefile for robot project
CXXFLAGS += -std=c++11

main: controller.cpp view.cpp main.cpp
	g++ $(CXXFLAGS) -o main main.cpp controller.cpp view.cpp
clear:
	-rm -rf *.o *~main